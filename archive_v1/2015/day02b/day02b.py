total = 0


def process_file(data_file):
    with open(data_file, 'r') as f:
        for line in f:
            process_data(line)


def process_data(data):
    global total
    x, y, z = [ int(i) for i in data.split("x") ]
    front_perimeter = (2 * x) + (2 * y)
    side_perimeter = (2 * y) + (2 * z)
    top_perimeter = (2 * x) + (2 * z)
    bow_length = x * y * z
    ribbon_length = min(front_perimeter, side_perimeter, top_perimeter) + bow_length
    print("Dimensions: " + data.strip() + ", needs this length of ribbon: " + str(ribbon_length))
    total += ribbon_length


#process_file("day02b_testdata.txt")
process_file("day02b_data.txt")

print("Total amount of ribbon needed: " + str(total))
