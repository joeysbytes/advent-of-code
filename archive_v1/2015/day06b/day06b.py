import re

PATTERN = re.compile("([\\d]+,[\\d]+)")
MAX_X = 999
MAX_Y = 999
# 0 = off, 1 or more = brightness level
lights = []


def initialize():
    # Initialize lights to off
    for x in range(0, MAX_X + 1):
        lights.append([])
        for y in range(0, MAX_Y + 1):
            lights[x].append(0)


def finalize():
    total_brightness = 0
    for x in range(0, MAX_X + 1):
        for y in range(0, MAX_Y + 1):
            total_brightness += lights[x][y]
    print("Total brightness: " + str(total_brightness))


def process_file(data_file):
    with open(data_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            process_data(line.strip())


def process_data(data):
    global lights
    x1, y1, x2, y2 = extract_coordinates(data)
    for x in range(x1, x2 + 1):
        for y in range(y1, y2 + 1):
            if data.startswith("turn on"):
                lights[x][y] += 1
            elif data.startswith("turn off"):
                lights[x][y] = max(0, lights[x][y] - 1)
            else:
                lights[x][y] += 2


def extract_coordinates(data):
    all_coordinates = PATTERN.findall(data)
    x1, y1 = [int(c) for c in all_coordinates[0].split(",")]
    x2, y2 = [int(c) for c in all_coordinates[1].split(",")]
    return x1, y1, x2, y2


initialize()
# process_file("day06b_testdata.txt")
process_file("day06b_data.txt")
finalize()
