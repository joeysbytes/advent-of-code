import hashlib


def process_file(data_file):
    with open(data_file, 'r') as f:
        for line in f:
            process_data(line)


def process_data(data):
    hash_not_found = True
    i = 0
    while hash_not_found:
        hex_digest = hashlib.md5((data.strip() + str(i)).encode('UTF-8')).hexdigest()
        if hex_digest[0:5] == "00000":
            print(i)
            hash_not_found = False
        i += 1


#process_file("day04a_testdata.txt")
process_file("day04a_data.txt")
