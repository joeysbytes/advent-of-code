naughty = 0
nice = 0

def process_file(data_file):
    with open(data_file, 'r') as f:
        for line in f:
            process_data(line.strip())


def process_data(data):
    global naughty, nice
    status = None
    if status is None:
        if ("ab" in data) or ("cd" in data) or ("pq" in data) or ("xy" in data):
            status = "Naughty"
    if status is None:
        vowel_count = data.count("a") + data.count("e") + data.count("i") + data.count("o") + data.count("u")
        if vowel_count < 3:
            status = "Naughty"
    if status is None:
        double_found = False
        previous_letter = None
        for letter in data:
            if letter == previous_letter:
                double_found = True
                break
            previous_letter = letter
        if not double_found:
            status = "Naughty"
    if status is None:
        status = "Nice"
    if status == "Nice":
        print("Nice: " + data)
        nice += 1
    else:
        print("Naughty: " + data)
        naughty += 1


# process_file("day05a_testdata.txt")
process_file("day05a_data.txt")

print("Total nice: " + str(nice))
