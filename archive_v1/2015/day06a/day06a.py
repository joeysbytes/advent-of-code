import re

PATTERN = re.compile("([\\d]+,[\\d]+)")
MAX_X = 999
MAX_Y = 999
# -1 = off, 1 = on
lights = []


def initialize():
    # Initialize lights to off
    for x in range(0, MAX_X + 1):
        lights.append([])
        for y in range(0, MAX_Y + 1):
            lights[x].append(-1)


def finalize():
    total_on = 0
    for x in range(0, MAX_X + 1):
        for y in range(0, MAX_Y + 1):
            if lights[x][y] == 1:
                total_on += 1
    print("Total lights turned on: " + str(total_on))


def process_file(data_file):
    with open(data_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            process_data(line.strip())


def process_data(data):
    global lights
    x1, y1, x2, y2 = extract_coordinates(data)
    for x in range(x1, x2 + 1):
        for y in range(y1, y2 + 1):
            if data.startswith("turn on"):
                lights[x][y] = 1
            elif data.startswith("turn off"):
                lights[x][y] = -1
            else:
                lights[x][y] = lights[x][y] * (-1)


def extract_coordinates(data):
    all_coordinates = PATTERN.findall(data)
    x1, y1 = [int(c) for c in all_coordinates[0].split(",")]
    x2, y2 = [int(c) for c in all_coordinates[1].split(",")]
    return x1, y1, x2, y2


initialize()
# process_file("day06a_testdata.txt")
process_file("day06a_data.txt")
finalize()
