def process_file(data_file):
    with open(data_file, 'r') as f:
        for line in f:
            process_data(line)


def process_data(data):
    floor = 0
    position = 0
    for step in data:
        position += 1
        if step == "(":
            floor += 1
        else:
            floor -= 1
        if floor == -1:
            print("First basement position: " + str(position))
            break


process_file("day01b_testdata.txt")
process_file("day01b_data.txt")
