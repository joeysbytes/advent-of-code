def process_file(data_file):
    with open(data_file, 'r') as f:
        for line in f:
            process_data(line)


def process_data(data):
    floor = data.count("(") - data.count(")")
    print("Floor: " + str(floor))


process_file("day01a_testdata.txt")
process_file("day01a_data.txt")
