total = 0


def process_file(data_file):
    with open(data_file, 'r') as f:
        for line in f:
            process_data(line)


def process_data(data):
    global total
    x, y, z = [ int(i) for i in data.split("x") ]
    front_area = x * y
    side_area = y * z
    top_area = x * z
    present_area = (2 * front_area) + (2 * side_area) + (2 * top_area)
    wrapping_paper = present_area + min(front_area, side_area, top_area)
    print("Wrapping dimensions: " + data.strip() + ", wrapping paper needed: " + str(wrapping_paper))
    total += wrapping_paper


#process_file("day02a_testdata.txt")
process_file("day02a_data.txt")

print("Total wrapping paper needed: " + str(total))