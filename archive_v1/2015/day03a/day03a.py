def process_file(data_file):
    with open(data_file, 'r') as f:
        for line in f:
            process_data(line)


def process_data(data):
    houses = set()
    x = 0
    y = 0
    houses.add((x, y))
    for direction in data:
        if direction == "^":
            y += 1
        elif direction == "v":
            y -= 1
        elif direction == "<":
            x -= 1
        elif direction == ">":
            x += 1
        houses.add((x, y))
    print("Unique houses: " + str(len(houses)))


#process_file("day03a_testdata.txt")
process_file("day03a_data.txt")
