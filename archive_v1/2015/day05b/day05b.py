naughty = 0
nice = 0


def process_file(data_file):
    with open(data_file, 'r') as f:
        for line in f:
            process_data(line.strip())


def process_data(data):
    global naughty, nice
    two_pairs_found = False
    spaced_pair_found = False
    data_len = len(data)
    for i in range(0, data_len - 3):
        for j in range(i + 2, data_len - 1):
            if data[i:i+2] == data[j:j+2]:
                two_pairs_found = True
                break
        if two_pairs_found:
            break
    if two_pairs_found:
        for i in range(0, data_len - 2):
            if data[i:i+1] == data[i+2:i+3]:
                spaced_pair_found = True
                break
    if two_pairs_found and spaced_pair_found:
        print("Nice: " + data)
        nice += 1
    else:
        print("Naughty: " + data)
        naughty += 1


# process_file("day05b_testdata.txt")
process_file("day05b_data.txt")

print("Total nice: " + str(nice))
