def process_file(data_file):
    with open(data_file, 'r') as f:
        for line in f:
            process_data(line)


def process_data(data):
    houses = set()
    sleigh = 0
    x = [0, 0]
    y = [0, 0]
    houses.add((x[0], y[0]))
    for direction in data:
        if direction == "^":
            y[sleigh] += 1
        elif direction == "v":
            y[sleigh] -= 1
        elif direction == "<":
            x[sleigh] -= 1
        elif direction == ">":
            x[sleigh] += 1
        houses.add((x[sleigh], y[sleigh]))
        sleigh = abs(sleigh - 1)
    print("Unique houses: " + str(len(houses)))


#process_file("day03b_testdata.txt")
process_file("day03b_data.txt")
