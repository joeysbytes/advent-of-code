#!/bin/bash
set -e

YEAR="2015"
BASE_DIR="/home/joey/Development/advent-of-code"

if [ $# -ne 1 ]
then
  echo "USAGE:   new_day.sh ##x (day number)"
  echo "Example: new_day.sh 05b"
  exit 1
fi

DAY_NUM="day${1}"
echo "Making day: ${YEAR} - ${DAY_NUM}"

DAY_SUFFIX="${DAY_NUM:5:1}"
DAY_DIR="${BASE_DIR}/${YEAR}/${DAY_NUM}"
mkdir "${DAY_DIR}"

if [ "${DAY_SUFFIX}" == "b" ]
then
  A_DAY_NUM="${DAY_NUM:0:5}a"
  A_DAY_DIR="${BASE_DIR}/${YEAR}/${A_DAY_NUM}"
  cp --no-clobber "${A_DAY_DIR}/${A_DAY_NUM}.py" "${DAY_DIR}/${DAY_NUM}.py"
  cp --no-clobber "${A_DAY_DIR}/${A_DAY_NUM}_data.txt" "${DAY_DIR}/${DAY_NUM}_data.txt"
  cp --no-clobber "${A_DAY_DIR}/${A_DAY_NUM}_testdata.txt" "${DAY_DIR}/${DAY_NUM}_testdata.txt"
  sed -i "s/${A_DAY_NUM}/${DAY_NUM}/g" "${DAY_DIR}/${DAY_NUM}.py"
else
  touch "${DAY_DIR}/${DAY_NUM}_data.txt"
  touch "${DAY_DIR}/${DAY_NUM}_testdata.txt"
  cp --no-clobber "./template.py" "${DAY_DIR}/${DAY_NUM}.py"
  sed -i "s/day00x/${DAY_NUM}/g" "${DAY_DIR}/${DAY_NUM}.py"
fi
