def initialize():
    pass


def finalize():
    pass


def process_file(data_file):
    with open(data_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            process_data(line.strip())


def process_data(data):
    pass


initialize()
process_file("day00x_testdata.txt")
# process_file("day00x_data.txt")
finalize()
