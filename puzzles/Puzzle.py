from abc import ABC, abstractmethod
import time


class Puzzle(ABC):

    def __init__(self, data_file: str):
        self.data = None
        self.answer = None
        self.start_time = None
        self.end_time = None
        self.data_file = data_file

    @abstractmethod
    def process(self) -> None:
        """This begins the actual processing of the puzzle.
        It should end by populating a value in self.answer."""
        raise NotImplementedError

    def run(self) -> None:
        """This performs pre and post work, as well as runs the puzzle."""
        self.load_data_file()
        self.start_time = time.time_ns()
        self.process()
        self.end_time = time.time_ns()
        self.print_answer()
        self.print_elapsed_time()

    def load_data_file(self) -> None:
        """This loads the data file in to the data variable."""
        print(f"Loading data file: {self.data_file}")
        self.data = []
        with open(self.data_file, 'r') as f:
            for line in f:
                # strip off the ending newline
                data = line[0:-1]
                self.data.append(data)

    def print_answer(self) -> None:
        """Print the value of answer."""
        print()
        print(f"Answer: {self.answer}")

    def print_elapsed_time(self) -> None:
        """Print how long processing took."""
        ns_in_ms = 1000000
        ms_in_sec = 1000

        elapsed_time_ns = self.end_time - self.start_time
        elapsed_time_ms = elapsed_time_ns // ns_in_ms

        elapsed_time_seconds = str(elapsed_time_ms // ms_in_sec)
        elapsed_time_milliseconds = "{:0>3}".format(elapsed_time_ms % ms_in_sec)

        print(f"Elapsed time (secs): {elapsed_time_seconds}.{elapsed_time_milliseconds}")
