from puzzles.Puzzle import Puzzle


class Day02a(Puzzle):

    def process(self) -> None:
        self.square_ft = 0
        for line in self.data:
            self.calc_square_feet(line)
        self.answer = self.square_ft

    def calc_square_feet(self, data):
        length, width, height = data.split("x")
        length, width, height = int(length), int(width), int(height)
        side_1 = length * width
        side_2 = length * height
        side_3 = width * height
        min_side = min(side_1, side_2, side_3)
        sq_ft = (2 * side_1) + (2 * side_2) + (2 * side_3) + min_side
        self.square_ft += sq_ft
