from puzzles.Puzzle import Puzzle


class Day02b(Puzzle):

    def process(self) -> None:
        self.length_ft = 0
        for line in self.data:
            self.calc_length_feet(line)
        self.answer = self.length_ft

    def calc_length_feet(self, data):
        length, width, height = data.split("x")
        length, width, height = int(length), int(width), int(height)
        perimeter_1 = (length * 2) + (width * 2)
        perimeter_2 = (length * 2) + (height * 2)
        perimeter_3 = (width * 2) + (height * 2)
        small_perimeter = min(perimeter_1, perimeter_2, perimeter_3)
        bow = length * width * height
        len_ft = small_perimeter + bow
        self.length_ft += len_ft
