from puzzles.Puzzle import Puzzle


class Day01b(Puzzle):

    TARGET_FLOOR = -1

    def process(self) -> None:
        for line in self.data:
            floor = 0
            character_position = 0
            for instruction in line:
                character_position += 1
                if instruction == "(":
                    floor += 1
                elif instruction == ")":
                    floor -= 1
                if floor == Day01b.TARGET_FLOOR:
                    self.answer = character_position
                    break
