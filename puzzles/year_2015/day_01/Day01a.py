from puzzles.Puzzle import Puzzle


class Day01a(Puzzle):

    def process(self) -> None:
        for line in self.data:
            floor = 0
            for instruction in line:
                if instruction == "(":
                    floor += 1
                elif instruction == ")":
                    floor -= 1
            print(f"Floor: {floor}")
            self.answer = floor
