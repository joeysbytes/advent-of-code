LIGHT_GRID = None
MAX_ROWS = 1000
MAX_COLS = 1000

###########################################################################
# Initialization
###########################################################################
def initialize():
    global LIGHT_GRID
    LIGHT_GRID = []
    for x in range(0, MAX_ROWS):
        LIGHT_GRID.append([])
        for y in range(0, MAX_COLS):
            LIGHT_GRID[x].append(0)


###########################################################################
# Finalization
###########################################################################
def finalize():
    count = 0
    for x in range(0, MAX_ROWS):
        for y in range(0, MAX_COLS):
            count += LIGHT_GRID[x][y]
    print(f"Lit Lights: {count}")


###########################################################################
# Processing
###########################################################################
def process_file(input_file):
    print(f"Processing file: {input_file}")
    with open(input_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            data = line.strip()
            if len(data) > 0:
                process_data(data)


def process_data(data):
    command = parse_command(data)
    process_command(command)


def process_command(command):
    global LIGHT_GRID
    action = command["action"]
    for x in range(command["start_x"], command["end_x"]+1):
        for y in range(command["start_y"], command["end_y"]+1):
            if action == "turn on":
                LIGHT_GRID[x][y] = 1
            elif action == "turn off":
                LIGHT_GRID[x][y] = 0
            elif action == "toggle":
                LIGHT_GRID[x][y] = 0 if LIGHT_GRID[x][y] == 1 else 1
            else:
                raise RuntimeError(f"Unknown action: {action}")


def parse_command(full_command):
    command = {"action": None,
               "start_x": None,
               "start_y": None,
               "end_x": None,
               "end_y": None}

    command_parts = full_command.split(" ")
    idx = 0

    # get command
    if command_parts[idx] == "turn":
        command["action"] = f"{command_parts[idx]} {command_parts[idx+1]}"
        idx += 2
    else:
        command["action"] = command_parts[idx]
        idx += 1

    # get start x,y coordinates
    start_coordinates = command_parts[idx].split(",")
    command["start_x"] = int(start_coordinates[0])
    command["start_y"] = int(start_coordinates[1])
    idx += 2

    # get end x,y coordinates
    end_coordinates = command_parts[idx].split(",")
    command["end_x"] = int(end_coordinates[0])
    command["end_y"] = int(end_coordinates[1])

    return command


###########################################################################
# Start the script
###########################################################################
initialize()
# process_file("example_input.txt")  # 998996
process_file("input.txt")
finalize()
