COUNT = 0

###########################################################################
# Initialization
###########################################################################
def initialize():
    pass


###########################################################################
# Finalization
###########################################################################
def finalize():
    print(f"Nice count: {COUNT}")


###########################################################################
# Processing
###########################################################################
def process_file(input_file):
    print(f"Processing file: {input_file}")
    with open(input_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            data = line.strip()
            if len(data) > 0:
                process_data(data)


def process_data(data):
    global COUNT
    pair_of_letters = False
    separated_pair = False

    # It contains a pair of any two letters that appears at least twice in the string without overlapping,
    # like xyxy (xy) or aabcdefgaa (aa), but not like aaa (aa, but it overlaps)
    for idx in range(0, len(data) - 1):
        current_pair = data[idx:idx+2]
        rest_of_data = data[idx+2:]
        if current_pair in rest_of_data:
            pair_of_letters = True
            break

    # It contains at least one letter which repeats with exactly one letter between them,
    # like xyx, abcdefeghi (efe), or even aaa
    if pair_of_letters:
        for idx in range(0, len(data) - 2):
            first_char = data[idx:idx+1]
            second_char = data[idx+2:idx+3]
            if first_char == second_char:
                separated_pair = True
                break

    if pair_of_letters and separated_pair:
        COUNT += 1


###########################################################################
# Start the script
###########################################################################
initialize()
# process_file("example_input.txt")  # 2
process_file("../day_05a/input.txt")
finalize()
