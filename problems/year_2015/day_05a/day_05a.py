COUNT = 0

###########################################################################
# Initialization
###########################################################################
def initialize():
    pass


###########################################################################
# Finalization
###########################################################################
def finalize():
    print(f"Nice count: {COUNT}")


###########################################################################
# Processing
###########################################################################
def process_file(input_file):
    print(f"Processing file: {input_file}")
    with open(input_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            data = line.strip()
            if len(data) > 0:
                process_data(data)


def process_data(data):
    global COUNT
    vowel_count = 0
    double_letter = False
    previous_char = ""
    invalid_sequence = False

    for idx in range(0, len(data)):
        current_char = data[idx:idx+1]
        # It contains at least three vowels (aeiou only)
        if current_char in ["a", "e", "i", "o", "u"]:
            vowel_count += 1

        # It contains at least one letter that appears twice in a row, like xx
        if current_char == previous_char:
            double_letter = True
        previous_char = current_char

        # It does not contain the strings ab, cd, pq, or xy
        if idx < len(data) - 1:
            two_chars = data[idx:idx+2]
            if two_chars in ["ab", "cd", "pq", "xy"]:
                invalid_sequence = True
                break

    if (not invalid_sequence) and (vowel_count >= 3) and (double_letter):
        COUNT += 1


###########################################################################
# Start the script
###########################################################################
initialize()
# process_file("example_input.txt")  # 2
process_file("input.txt")
finalize()
