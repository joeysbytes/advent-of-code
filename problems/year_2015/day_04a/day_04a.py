import hashlib


###########################################################################
# Initialization
###########################################################################
def initialize():
    pass


###########################################################################
# Finalization
###########################################################################
def finalize():
    pass


###########################################################################
# Processing
###########################################################################
def process_file(input_file):
    print(f"Processing file: {input_file}")
    with open(input_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            data = line.strip()
            if len(data) > 0:
                process_data(data)


def process_data(data):
    starts_with = "00000"
    print(f"Secret Key: {data}")

    num = 0
    while True:
        if num % 10000 == 0:
            print(f"  Checking: {num}")
        enc_num = f"{data}{num}"
        md5hash = hashlib.md5(enc_num.encode()).hexdigest()
        if md5hash.startswith(starts_with):
            print(f"--> Lowest Number: {num} <--")
            break
        num += 1


###########################################################################
# Start the script
###########################################################################
initialize()
# process_file("example_input.txt")  # 609043, 1048970
process_file("input.txt")
finalize()
