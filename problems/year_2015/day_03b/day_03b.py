###########################################################################
# Initialization
###########################################################################
def initialize():
    pass


###########################################################################
# Finalization
###########################################################################
def finalize():
    pass


###########################################################################
# Processing
###########################################################################
def process_file(input_file):
    print(f"Processing file: {input_file}")
    with open(input_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            data = line.strip()
            if len(data) > 0:
                process_data(data)


def process_data(data):
    # starting location x, y, quantity of 1
    house_grid = dict()
    house_grid[(0, 0)] = 2
    grid_locations = [[0, 0], [0, 0]]

    for idx in range(0, len(data)):
        direction = data[idx:idx + 1]
        grid_location = grid_locations[idx % 2]

        if direction == "^":  # north
            grid_location[1] -= 1
        elif direction == "v":  # south
            grid_location[1] += 1
        elif direction == "<":  # west
            grid_location[0] -= 1
        elif direction == ">":  # east
            grid_location[0] += 1
        grid_key = tuple(grid_location)
        if grid_key in house_grid.keys():
            house_grid[grid_key] += 1
        else:
            house_grid[grid_key] = 1

    print(f"Num houses: {len(house_grid)}")


###########################################################################
# Start the script
###########################################################################
initialize()
# process_file("example_input.txt")  # 3, 3, 11
process_file("../day_03a/input.txt")
finalize()
