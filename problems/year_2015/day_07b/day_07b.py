VARIABLES = {}
COMMANDS = {}  # key = destination, value = command
PROCESSED_COUNT = 0

###########################################################################
# Initialization
###########################################################################
def initialize():
    pass


###########################################################################
# Finalization
###########################################################################
def finalize():
    global VARIABLES, COMMANDS, PROCESSED_COUNT
    process_commands()

    new_b = VARIABLES['a']
    VARIABLES = {}
    PROCESSED_COUNT = 0
    for dest, command in COMMANDS.items():
        command["processed"] = False
        if command["dest"] == "b":
            cmd = parse_command(f"{new_b} -> b")
            COMMANDS[command["dest"]] = cmd
    process_commands()

    print("Variables Dictionary")
    print("====================")
    if "a" in VARIABLES:
        print()
        print(f"a wire: {VARIABLES['a']}")


###########################################################################
# Processing
###########################################################################
def process_file(input_file):
    print(f"Processing file: {input_file}")
    with open(input_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            data = line.strip()
            if len(data) > 0:
                process_data(data)


def process_data(data):
    command = parse_command(data)
    COMMANDS[command["dest"]] = command


def process_commands():
    while PROCESSED_COUNT < len(COMMANDS):
        for dest, command in COMMANDS.items():
            if not command["processed"]:
                process_command(command)


def process_command(command):
    # If the destination variable already exists, something is wrong
    if command["dest"] in VARIABLES:
        raise RuntimeError(f"Variable already exists: {command['dest']}")

    action = command["action"]
    if action == "ASSIGN":
        command_assign(command)
    elif action == "AND":
        command_and(command)
    elif action == "OR":
        command_or(command)
    elif action == "NOT":
        command_not(command)
    elif action == "LSHIFT":
        command_lshift(command)
    elif action == "RSHIFT":
        command_rshift(command)
    else:
        raise RuntimeError(f"Unknown Command: {command}")


def command_assign(command):
    value = get_value(command["src_1"])
    if value is not None:
        store_value(command, value)


def command_not(command):
    value_1 = get_value(command["src_1"])
    if value_1 is not None:
        value_2 = to_16bit(~ value_1)
        store_value(command, value_2)


def command_and(command):
    value_2 = None
    value_1 = get_value(command["src_1"])
    if value_1 is not None:
        value_2 = get_value(command["src_2"])
    if value_1 is not None and value_2 is not None:
        value_3 = to_16bit(value_1 & value_2)
        store_value(command, value_3)


def command_or(command):
    value_2 = None
    value_1 = get_value(command["src_1"])
    if value_1 is not None:
        value_2 = get_value(command["src_2"])
    if value_1 is not None and value_2 is not None:
        value_3 = to_16bit(value_1 | value_2)
        store_value(command, value_3)


def command_lshift(command):
    value_2 = None
    value_1 = get_value(command["src_1"])
    if value_1 is not None:
        value_2 = get_value(command["src_2"])
    if value_1 is not None and value_2 is not None:
        value_3 = to_16bit(value_1 << value_2)
        store_value(command, value_3)


def command_rshift(command):
    value_2 = None
    value_1 = get_value(command["src_1"])
    if value_1 is not None:
        value_2 = get_value(command["src_2"])
    if value_1 is not None and value_2 is not None:
        value_3 = to_16bit(value_1 >> value_2)
        store_value(command, value_3)


def to_16bit(num):
    return num & 0xFFFF


def get_value(variable_name):
    # If a number is passed in, just return the number.
    if variable_name.isdigit():
        return to_16bit(int(variable_name))
    if variable_name in VARIABLES:
        return VARIABLES[variable_name]
    # If the value is not in variables, return None
    return None


def store_value(command, value):
    global PROCESSED_COUNT
    VARIABLES[command["dest"]] = value
    command["processed"] = True
    PROCESSED_COUNT += 1


def parse_command(command):
    cmd = {"processed": False}
    command_parts = command.split(" ")
    command_parts_len = len(command_parts)

    if (command_parts_len == 3) and (command_parts[1] == "->"):
        # Straight assignment
        cmd["action"] = "ASSIGN"
        cmd["src_1"] = command_parts[0]
        cmd["dest"] = command_parts[2]
    elif (command_parts_len == 4) and (command_parts[2] == "->"):
        # NOT
        cmd["action"] = command_parts[0]
        cmd["src_1"] = command_parts[1]
        cmd["dest"] = command_parts[3]
    elif (command_parts_len == 5) and (command_parts[3] == "->"):
        # AND, OR, LSHIFT, RSHIFT
        cmd["action"] = command_parts[1]
        cmd["src_1"] = command_parts[0]
        cmd["src_2"] = command_parts[2]
        cmd["dest"] = command_parts[4]
    else:
        raise RuntimeError(f"Unknown command type: {command}")

    return cmd


###########################################################################
# Start the script
###########################################################################
initialize()
# process_file("../day_07a/test_input.txt")
# process_file("../day_07a/example_input.txt")
process_file("../day_07a/input.txt")
finalize()
