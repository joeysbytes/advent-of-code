import re

SUM = 0
DIGIT_PATTERN = re.compile(r"[0-9]{1}")


###########################################################################
# Initialization
###########################################################################
def initialize():
    pass


###########################################################################
# Finalization
###########################################################################
def finalize():
    print(f"Sum = {SUM}")


###########################################################################
# Processing
###########################################################################
def process_file(input_file):
    print(f"Processing file: {input_file}")
    with open(input_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            data = line.strip()
            if len(data) > 0:
                process_data(data)


def process_data(data):
    global SUM
    all_digits = re.findall(DIGIT_PATTERN, data)
    first_digit = all_digits[0]
    last_digit = all_digits[-1]
    number = (int(first_digit) * 10) + int(last_digit)
    SUM += number


###########################################################################
# Start the script
###########################################################################
initialize()
# process_file("example_input.txt")  # 142
process_file("input.txt")
finalize()
