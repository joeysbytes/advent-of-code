SUM = 0
DICE_BAG = {"red": 12,
            "green": 13,
            "blue": 14}


###########################################################################
# Initialization
###########################################################################
def initialize():
    pass


###########################################################################
# Finalization
###########################################################################
def finalize():
    print(f"Sum of Game IDs: {SUM}")


###########################################################################
# Processing
###########################################################################
def process_file(input_file):
    print(f"Processing file: {input_file}")
    with open(input_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            data = line.strip()
            if len(data) > 0:
                process_data(data)


def process_data(data):
    global SUM
    is_valid_game = True

    game_data_parts = data.split(":")
    game_id = int(game_data_parts[0].split(" ")[1])

    game_plays = game_data_parts[1].split(";")
    for game_play in game_plays:
        dice = game_play.split(",")
        for die in dice:
            die_parts = die.split(" ")
            quantity = int(die_parts[1])
            color = die_parts[2]
            if DICE_BAG[color] < quantity:
                is_valid_game = False
                break
        if not is_valid_game:
            break
    if is_valid_game:
        SUM += game_id


###########################################################################
# Start the script
###########################################################################
initialize()
# process_file("example_input.txt")  # 8
process_file("input.txt")
finalize()
