SUM = 0


###########################################################################
# Initialization
###########################################################################
def initialize():
    pass


###########################################################################
# Finalization
###########################################################################
def finalize():
    print(f"Sum of Power: {SUM}")


###########################################################################
# Processing
###########################################################################
def process_file(input_file):
    print(f"Processing file: {input_file}")
    with open(input_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            data = line.strip()
            if len(data) > 0:
                process_data(data)


def process_data(data):
    global SUM
    dice_bag = {"red": 0,
                "green": 0,
                "blue": 0}

    game_data_parts = data.split(":")
    game_id = int(game_data_parts[0].split(" ")[1])

    game_plays = game_data_parts[1].split(";")
    for game_play in game_plays:
        dice = game_play.split(",")
        for die in dice:
            die_parts = die.split(" ")
            quantity = int(die_parts[1])
            color = die_parts[2]
            dice_bag[color] = max(quantity, dice_bag[color])
    power = dice_bag["red"] * dice_bag["green"] * dice_bag["blue"]
    SUM += power


###########################################################################
# Start the script
###########################################################################
initialize()
# process_file("../day_02a/example_input.txt")  # 48 + 12 + 1560 + 630 + 36 = 2286
process_file("../day_02a/input.txt")
finalize()
