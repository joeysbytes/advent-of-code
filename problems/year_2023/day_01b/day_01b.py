SUM = 0


###########################################################################
# Initialization
###########################################################################
def initialize():
    pass


###########################################################################
# Finalization
###########################################################################
def finalize():
    print(f"Sum = {SUM}")


###########################################################################
# Processing
###########################################################################
def process_file(input_file):
    print(f"Processing file: {input_file}")
    with open(input_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            data = line.strip()
            if len(data) > 0:
                process_data(data)


def process_data(data):
    global SUM
    first_digit = None
    last_digit = None

    # loop through each character in data
    idx = 0
    while idx < len(data):
        current_digit, idx_increment = get_single_digit(idx, data)
        if idx_increment == 0:
            current_digit, idx_increment = get_digit_word(idx, data)
        if idx_increment > 0:
            last_digit = current_digit
            if first_digit is None:
                first_digit = current_digit
            idx += idx_increment
        else:
            idx += 1
    number = (int(first_digit) * 10) + int(last_digit)
    SUM += number


def get_single_digit(idx, data):
    """Is the character at the current index a digit?"""
    end_idx = idx + 1
    if data[idx:end_idx].isdigit():
        return data[idx:end_idx], 1
    return None, 0


def get_digit_word(idx, data):
    """Are the characters at the current index a digit word?"""
    digit_dict = {"one": "1",
                  "two": "2",
                  "three": "3",
                  "four": "4",
                  "five": "5",
                  "six": "6",
                  "seven": "7",
                  "eight": "8",
                  "nine": "9"}
    data_len = len(data) - idx

    for digit_word, digit_num in digit_dict.items():
        digit_word_len = len(digit_word)
        if data_len >= digit_word_len:
            end_idx = idx + digit_word_len
            if data[idx:end_idx] == digit_word:
                return digit_num, digit_word_len
    return None, 0


###########################################################################
# Start the script
###########################################################################
initialize()
# process_file("example_input.txt")  # 281
process_file("../day_01a/input.txt")
finalize()
