###########################################################################
# Initialization
###########################################################################
def initialize():
    pass


###########################################################################
# Finalization
###########################################################################
def finalize():
    pass


###########################################################################
# Processing
###########################################################################
def process_file(input_file):
    print(f"Processing file: {input_file}")
    with open(input_file, 'r') as f:
        for line in f:
            # strip off the ending newline
            data = line.strip()
            if len(data) > 0:
                process_data(data)


def process_data(data):
    pass


###########################################################################
# Start the script
###########################################################################
initialize()
process_file("example_input.txt")
# process_file("input.txt")
finalize()
