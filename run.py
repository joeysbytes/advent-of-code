# Advent of Code Puzzle Runner
# https://adventofcode.com

# Puzzle to run
YEAR = "2015"
DAY = "02"
DAY_PART = "b"
DATA_FILE = "input.txt"


def main():
    """Main processing logic for running a puzzle."""
    print_heading()
    data_file_name = f"./files/year_{YEAR}/day_{DAY}/{DATA_FILE}"
    PuzzleClass = get_puzzle_class()
    puzzle_class = PuzzleClass(data_file_name)
    puzzle_class.run()


def get_puzzle_class():
    """Import and return the actual puzzle class to run."""
    class_name = f"Day{DAY}{DAY_PART}"
    module_name = f"puzzles.year_{YEAR}.day_{DAY}.{class_name}.{class_name}"
    module_groups = module_name.split(".")
    module = ".".join(module_groups[:-1])
    b = __import__(module)
    for comp in module_groups[1:]:
        b = getattr(b, comp)
    return b


def print_heading():
    print("==============")
    print("Advent of Code")
    print("==============")
    print(f"Year: {YEAR}, Day: {DAY}{DAY_PART}")
    print()


# Start the script
main()
